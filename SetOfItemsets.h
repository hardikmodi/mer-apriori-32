/* 
 * File:   ListOfItemsets.h
 * Author: aeon
 *
 * Created on 5 February, 2013, 6:59 PM
 */

#include <set>
#include <list>
using namespace std;

#ifndef LISTOFITEMSETS_H
#define	LISTOFITEMSETS_H

class SetOfItemsets {
        private:
        set< int > s;
        int getIndex(set<int> &);        
    public:
        SetOfItemsets(){};        
        virtual ~SetOfItemsets(){};          
        bool subsetCheck(set<int> &);
        bool add(set<int> &);
        bool isIntersection(const int a, const int b);
        long int cardinality(void);
        set<int>::iterator first(void); 
        set<int>::iterator last(void);         
        void display(void);
        set<int> setRep;
        void getSet(int);  
        void stub(void);
};

#endif	/* LISTOFITEMSETS_H */

