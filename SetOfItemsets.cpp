/* 
 * File:   SetOfItemsets.cpp
 * Author: aeon
 * 
 * Created on 5 February, 2013, 6:59 PM
 */
#include <iostream>
#include <set>
#include "SetOfItemsets.h"

bool SetOfItemsets::subsetCheck(set<int> &x)
{    
   int key = getIndex(x);
   set<int>::iterator itr;
   for (itr=s.begin();itr!=s.end();itr++)
   {
       if (key == (key & (*itr)))
           return true;
   }
   return false;
}

bool SetOfItemsets::add(set<int> &x)
{
    int key = getIndex(x);
    return (s.insert(key)).second;
}

bool SetOfItemsets::isIntersection(const int a, const int b)
{
    if ((a&b)!=0)
        return true;
    else
        return false;
}

long int SetOfItemsets::cardinality(void)
{
    return s.size();
}

set< int >::iterator SetOfItemsets::first(void)
{
    return s.begin();
}

set< int >::iterator SetOfItemsets::last(void)
{
    return s.end();    
}

void SetOfItemsets::display(void)
{
    set<int>::iterator itr;
    for(itr=s.begin();itr!=s.end();itr++)
    {
        getSet(*itr);
        set<int>::iterator it;
        for (it=setRep.begin();it!=setRep.end();it++)
        {
            cout<<*it<<", ";
        }
        cout<<"\n";        
    }
}

void SetOfItemsets::getSet(int a)
{
    //cout<<"\nCalled get Set with "<<a;
    setRep.clear();
    for (int i=0;i<32;i++)
    {
        if ((((unsigned int)1 << i) & a) != 0)
                setRep.insert(i);
    }
   // stub();
    //cout<<"\texiting..";
}
int SetOfItemsets::getIndex(set<int> &x)
{
    int key;
    key=0;
    set<int>::iterator itr;
    for (itr=x.begin();itr!=x.end();itr++)
    {
        key = key | ((unsigned int)1 << (*itr));       
    }
    return key;
}


void SetOfItemsets::stub(void)
{/*
    set<int> l;
    l.insert(0);    
    l.insert(2); 
    add(l);
    cout<<"tera index "<<getIndex(l)<<endl;
    for (int j=0;j<10;j++)
    {
        getSet(j);
        set<int>::iterator it;
        cout<<"Set:: "<<j<<" :: ";
        for (it=setRep.begin();it!=setRep.end();it++)
        {
            cout<<*it<<", ";
        }
        cout<<"\n";
    }
    l.clear();
    l.insert(30);
    l.insert(29);
    add(l);
    getSet(getIndex(l));*/
    set<int>::iterator it;
    cout<<"Set:: ";
    for (it=setRep.begin();it!=setRep.end();it++)
    {
        cout<<*it<<", ";
    }
    cout<<"\n";/*
    cout<<"Answer: "<<isIntersection(5,1)<<"\n";
    cout<<"Size:: "<<cardinality();
    cout<<"\nDISPLAY::\n";
    display();
    l.clear();
    l.insert(30);
    cout<<"subsetCheck: "<<subsetCheck(l)<<"\n";
    cout<<"\n";*/
}